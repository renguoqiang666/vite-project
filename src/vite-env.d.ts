/// <reference types="vite/client" />
// 解决引入依赖报错(依赖引入报错是因为ts没有识别当前引入的依赖)
declare module 'file-saver'
declare module 'jszip-utils'
// vue3 报错提示 找不到模块“./XXX.vue”或其相应的类型声明
// 报错原因：typescript 只能理解 .ts 文件，无法理解 .vue文件
declare module '*.vue' {
  import { App, defineComponent } from 'vue'
  const component: ReturnType<typeof defineComponent> & {
    install(app: App): void
  }
  export default component
}
