/*
    * @description:
    * @param {Object} json 服务端发过来的数据
    * @param {String} name 导出Excel文件名字
   
    * @param {String} titleArr 导出Excel表头
   
    * @param {String} sheetName 导出sheetName名字
    * @return:
    */
import * as XLSX from 'xlsx'
export function exportExcel(json: Array<any>, name: string, titleArr: any, sheetName: any) {
  /* convert state to workbook */
  var data = new Array()
  var keyArray = new Array()
  const getLength = function (obj: { hasOwnProperty: (arg0: string) => any }) {
    var count = 0
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        count++
      }
    }
    return count
  }
  for (const key1 in json) {
    console.log(key1)
    if (json.hasOwnProperty(key1)) {
      const element = json[key1]
      var rowDataArray = new Array()
      for (const key2 in element) {
        if (element.hasOwnProperty(key2)) {
          const element2 = element[key2]
          rowDataArray.push(element2)
          if (keyArray.length < getLength(element)) {
            keyArray.push(key2)
          }
        }
      }
      data.push(rowDataArray)
    }
  }
  // json.forEach(item => {
  //   const values = Object.values(item)
  //   keyArray = Array.from(new Set([...keyArray, ...Object.keys(item)]))
  //   data.push(values)
  // })
  // keyArray为英文字段表头
  data.splice(0, 0, keyArray, titleArr)
  console.log('data', data)
  // XLSX.utils.aoa_to_sheet 是 XLSX 库中的一个方法，用于将二维数组转换成工作表对象;
  // data 是一个二维数组，表示 Excel 表格中的数据;
  const ws = XLSX.utils.aoa_to_sheet(data)
  // XLSX.utils.book_new() 是 XLSX 库中的一个方法，用于创建一个新的工作簿对象；
  const wb = XLSX.utils.book_new()
  // 此处隐藏英文字段表头
  var wsrows = [{ hidden: true }]
  ws['!rows'] = wsrows // ws - worksheet
  // XLSX.utils.book_append_sheet 是 XLSX 库中的一个方法，用于将一个工作表对象添加到工作簿对象中；
  // wb 是一个工作簿对象，ws 是一个工作表对象，sheetName 是要添加的工作表的名称；
  XLSX.utils.book_append_sheet(wb, ws, sheetName)
  // XLSX.writeFile 方法将工作簿对象保存为 Excel 文件。
  XLSX.writeFile(wb, name + '.xlsx')
}
