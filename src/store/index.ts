import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { learnMenuList } from '@/utils/menu'
const defaultState = {
  demo: localStorage.getItem('demo') || '我是测试的数据',
  count: 0,
  name: '从0到1搭建的项目',
  token: localStorage.getItem('token') || '',
  userName: localStorage.getItem('userName') || '',
  activeMenu: '1', // 当前激活的菜单
  menuList: learnMenuList // 菜单列表
}

// Create a new store instance.
export default createStore({
  state() {
    return defaultState
  },
  mutations: {
    increment(state: typeof defaultState) {
      state.count++
    },
    setDemo(state, val) {
      state.demo = val
    },
    setActiveMenu(state, val) {
      state.activeMenu = val
    },
    setUserName(state, val) {
      state.userName = val
    },
    setName(state, name) {
      state.name = name
    },
    setMenuList(state, val) {
      state.menuList = val
    }
  },
  actions: {
    increment(context) {
      context.commit('increment')
    }
  },
  getters: {
    double(state: typeof defaultState) {
      return 2 * state.count
    }
  },
  // vuex持久化配置
  plugins: [
    createPersistedState({
      // 存储方式：localStorage、sessionStorage、cookies
      storage: window.localStorage,
      // 存储的 key 的key值
      key: 'store',
      paths: ['activeMenu', 'menuList']
    })
  ]
})
