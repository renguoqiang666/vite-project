// event.js
// 组合式函数
// “组合式函数”(Composables) 是一个利用 Vue 的组合式 API 来封装和复用有状态逻辑的函数。
import { onMounted, onUnmounted } from 'vue'

export function useEventListener(
  target: { addEventListener: (arg0: any, arg1: any) => any; removeEventListener: (arg0: any, arg1: any) => any },
  event: any,
  callback: any
) {
  // 如果你想的话，
  // 也可以用字符串形式的 CSS 选择器来寻找目标 DOM 元素
  onMounted(() => target.addEventListener(event, callback))
  onUnmounted(() => target.removeEventListener(event, callback))
}
