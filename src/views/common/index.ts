export const learnList = [
  {
    title: 'JS中 new Date() 各方法的用法',
    url: 'https://blog.csdn.net/xxxxxxxx00772299/article/details/126451348',
    date: '2022-03-04'
  },
  {
    title: '手把手教学：VUE3+TS组件库开发与发布',
    url: 'https://huaweicloud.csdn.net/639fea8ddacf622b8df8ff6e.html?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-7-120831892-blog-126391327.pc_relevant_multi_platform_whitelistv3&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-7-120831892-blog-126391327.pc_relevant_multi_platform_whitelistv3&utm_relevant_index=14',
    date: '2022-03-04'
  },
  {
    title: 'Vue2 和 Vue3 的响应式原理比对',
    url: 'https://juejin.cn/post/7124351370521477128',
    date: '2022-03-04'
  },
  {
    title: '前端布局之浅谈BFC',
    url: 'https://juejin.cn/post/7149398890008018974/',
    date: '2022-03-04'
  },
  {
    title: '前端页面之“回流重绘”',
    url: 'https://juejin.cn/post/7150084386212610056',
    date: '2022-03-04'
  },
  {
    title: '学会SASS和LESS语法',
    url: 'https://juejin.cn/post/6875576860462448648',
    date: '2022-03-04'
  },
  {
    title: 'h5,c3的新特性',
    url: 'https://juejin.cn/post/7087921632265633805',
    date: '2022-03-04'
  },
  {
    title: 'ECMAScript 新特性语法，一次性掌握 ES6+',
    url: 'https://juejin.cn/post/6996546764295634980',
    date: '2022-03-04'
  },
  {
    title: '深入理解JS作用域和作用域链',
    url: 'https://juejin.cn/post/7096818495450513445',
    date: '2022-03-04'
  },
  {
    title: '数组、链表、队列和栈，四大基础数据结构详解',
    url: 'https://juejin.cn/post/6996815834534379557',
    date: '2022-03-04'
  },
  {
    title: '刨析Scoped原理',
    url: 'https://juejin.cn/post/7061588275139444766',
    date: '2022-03-04'
  }
]
