import { defineStore } from 'pinia'
import useAge from './age'
const useDemo = defineStore('demo', {
  state: () => ({
    count: localStorage.getItem('storeCount') || 1,
    name: '我是pinia中的名字',
    number: 10
  }),
  actions: {
    changeName(newName: string) {
      this.name = newName
      this.changeDemo()
    },
    changeAge() {
      const useAgeStore = useAge()
      useAgeStore.changeAge(22)
    },
    changeDemo() {
      console.log('ooo')
    }
  },
  // 开始数据持久化
  persist: {
    key: 'pinia',
    paths: ['name', 'number']
  }
})
export default useDemo
