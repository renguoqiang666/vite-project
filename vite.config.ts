import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path, { resolve } from 'path'
import { viteMockServe } from 'vite-plugin-mock'

// https://vitejs.dev/config/
export default ({ command }) => {
  return {
    plugins: [
      vue(),
      viteMockServe({
        localEnabled: command === 'serve'
      })
    ],
    resolve: {
      alias: {
        '@': resolve(__dirname, 'src') // 设置 `@` 指向 `src` 目录
      }
    },
    css: {
      // 解决定义的less变量报undefined
      preprocessorOptions: {
        less: {
          modifyVars: {
            hack: `true; @import (reference) "${path.resolve('src/style/common.less')}";`
          },
          javascriptEnabled: true
        }
      }
    },
    base: '/vite-project',
    // 指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 将监听所有地址，包括局域网和公网地址。
    server: {
      host: '0.0.0.0',
      proxy: {
        '/api': {
          target: 'http://apis.juhe.cn/',
          changeOrigin: true,
          rewrite: path => path.replace(/^\/api/, '')
        },
        '/news': {
          target: 'http://v.juhe.cn/', // /news/toutiao/index http://v.juhe.cn/toutiao/index
          changeOrigin: true,
          rewrite: path => path.replace(/^\/news/, '')
        },
        '/ceshi': {
          target: 'https://cn.bing.com/',
          changeOrigin: true,
          rewrite: path => path.replace(/^\/ceshi/, '')
        }
      }
    }
  }
}

// export default defineConfig({

// })
