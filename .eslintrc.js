module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['eslint:recommended', 'plugin:vue/essential', 'plugin:@typescript-eslint/eslint-recommended'],
    globals: {
        defineEmits: true,
        defineProps: true,
        defineExpose: true
    },
    parserOptions: {
        ecmaVersion: 2018,
        parser: '@typescript-eslint/parser',
        sourceType: 'module'
    },
    plugins: ['vue', '@typescript-eslint'],
    rules: {
        indent: ['error', 4],
        'linebreak-style': ['error', 'windows'],
        quotes: ['error', 'single'],
        // 结尾不需要分号
        semi: ['off', 'never'],
        // 运算符号前后空一格
        'space-infix-ops': 'error',
        'vue/no-v-model-argument': 'off',
        // 每个单文件组件的模板（template）根元素只能有一个(关闭)
        'vue/no-multiple-template-root': 'off',
        // 在大括号前后都要添加空格
        'object-curly-spacing': ['error', 'always'],
        // html结束括号换号：关闭
        'vue/html-closing-bracket-newline': [
            0,
            {
                singleline: 'never',
                multiline: 'always'
            }
        ],
        // html缩进为两个空格
        'vue/html-indent': [
            'error',
            2,
            {
                attribute: 1, // 属性缩进的乘数
                baseIndent: 1, // 顶级语句的缩进倍数
                closeBracket: 0, // 右括号缩进的乘数
                alignAttributesVertically: true, // 在多行情况下，属性是否应与第一个属性垂直对齐的条件
                ignores: [] //  忽略节点的选择器
            }
        ],
        // html中引号强制使用双引号
        'vue/html-quotes': ['error', 'double'],
        // 正常标签内左右都不允许有空格，自闭合（单标签）标签可以存在一个或多个空格
        'vue/html-closing-bracket-spacing': [
            'error',
            {
                startTag: 'never',
                endTag: 'never',
                selfClosingTag: 'always'
            }
        ],
        // 标签中单行属性最多不能超过4个
        'vue/max-attributes-per-line': [
            'error',
            {
                singleline: 4,
            }
        ],
        // 需要在多行元素的内容前后换行
        'vue/multiline-html-element-content-newline': [
            0,
            {
                ignoreWhenEmpty: true,
                ignores: [
                    'a',
                    'b',
                    'u',
                    'span',
                    'img',
                    'input',
                    'strong',
                    'select',
                    'sub',
                    'sup',
                    'label',
                    'em',
                    'button',
                    'textarea',
                    'tt',
                    'var',
                    'samp',
                    'br',
                    'cite',
                    'code',
                    'font',
                    'strike'
                ]
            }
        ]
    }
}
