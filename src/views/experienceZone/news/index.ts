export interface newData {
  uniquekey: string
  title: string
  date: string
  category: string
  author_name: string
  url: string
  thumbnail_pic_s: string
  is_content: string
}
export interface news {
  data: newData[]
  page: string
  pageSize: string
  stat: string
}
