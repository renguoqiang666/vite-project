export interface MenuProp {
  id: string
  name: string
  route: string // 路由
  subMenuList?: MenuProp[] // 子集
}

export const learnMenuList = [
  {
    id: '1',
    name: '主页',
    route: '/index'
  },
  {
    id: '2',
    name: '干货',
    route: '/common'
  },
  {
    id: '3',
    name: 'JavaScript',
    route: '/JavaScript',
    subMenuList: [
      {
        id: '3-1',
        name: 'js基础',
        route: '/js/basic'
      }
    ]
  },
  {
    id: '4',
    name: 'TypeScript',
    route: '/TypeScript'
    // subMenuList: [
    //     {
    //         id: '2-1',
    //         name: '基础',
    //         route: '/axios'
    //     }
    // ]
  },
  {
    id: '5',
    name: 'vue',
    route: '/vue',
    subMenuList: [
      {
        id: '5-1',
        name: 'vue2',
        route: '/vue/vue2'
      },
      {
        id: '5-2',
        name: 'vue3',
        route: '/vue/vue3'
      },
      {
        id: '5-3',
        name: 'vuex',
        route: '/vue/vuex'
      },
      {
        id: '5-4',
        name: 'pinia',
        route: '/vue/pinia'
      }
    ]
  },
  {
    id: '6',
    name: 'Webpack',
    route: '/Webpack'
  },
  {
    id: '7',
    name: 'Vite',
    route: '/Vite'
  },
  {
    id: '8',
    name: 'Echarts',
    route: '/Echarts'
  },
  {
    id: '9',
    name: 'Git',
    route: '/Git'
  }
]
export const experienceMenuList = [
  {
    id: '1',
    name: '新闻头条',
    route: '/experience/news'
  },
  {
    id: '2',
    name: '拖拽',
    route: '/experience/dragg'
  },
  {
    id: '3',
    name: '魔方',
    route: '/experience/magic'
  },
  {
    id: '4',
    name: '表格导入导出Excel',
    route: '/experience/uploadAndExport'
  },
  {
    id: '5',
    name: 'Transition动画',
    route: '/experience/transition'
  },
  {
    id: '6',
    name: '动态组件',
    route: '/experience/trends'
  },
  {
    id: '7',
    name: '大表格',
    route: '/experience/bigTable'
  },
  {
    id: '8',
    name: '仿dialog',
    route: '/experience/dialog'
  },
  {
    id: '9',
    name: '富文本编辑器',
    route: '/experience/editor'
  },
  {
    id: '10',
    name: '随机抽奖',
    route: '/experience/random'
  },
  {
    id: '11',
    name: '小汽车',
    route: '/experience/car'
  },
  {
    id: '12',
    name: '复制粘贴图片文字',
    route: '/experience/imgPaste'
  },
  {
    id: '13',
    name: '导出PDF',
    route: '/experience/pdf'
  },
  {
    id: '14',
    name: '导出word',
    route: '/experience/word'
  }
]
