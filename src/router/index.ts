import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '@/views/home.vue'
import { ElMessage } from 'element-plus'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'Home',
        redirect: '/typeHome',
        component: Home,
        children: [
            {
                path: '/index',
                name: 'index',
                component: () => import('@/views/user/index.vue')
            },
            {
                path: '/common',
                name: 'common',
                component: () => import('@/views/common/index.vue')
            },
            {
                path: '/js/basic',
                name: 'jsBasic',
                component: () => import('@/views/jsModule/basic/index.vue')
            },
            {
                path: '/typeScript',
                name: 'typeScript',
                component: () => import('@/views/tsModule/index.vue')
            },
            {
                path: '/vue/vue2',
                name: 'vue2',
                component: () => import('@/views/vueModule/twoModule/index.vue')
                // meta: {
                //   keepAlive: true
                // }
            },

            {
                path: '/vue/vue3',
                name: 'vue3',
                component: () => import('@/views/vueModule/threeModule/index.vue')
                // meta: {
                //   keepAlive: true
                // }
            },
            {
                path: '/vue/vuex',
                name: 'vuex',
                component: () => import('@/views/vueModule/vuex/index.vue')
            },
            {
                path: '/vue/pinia',
                name: 'pinia',
                component: () => import('@/views/vueModule/pinia/index.vue')
            },
            {
                path: '/webpack',
                name: 'webpack',
                component: () => import('@/views/webpackModule/index.vue')
            },
            {
                path: '/vite',
                name: 'vite',
                component: () => import('@/views/viteModule/index.vue')
            },
            {
                path: '/echarts',
                name: 'echarts',
                component: () => import('@/views/echartsModule/index.vue')
            },
            {
                path: '/git',
                name: 'git',
                component: () => import('@/views/gitModule/index.vue')
            },
            {
                path: '/experience/dragg',
                name: 'dragg',
                component: () => import('@/views/experienceZone/dragg/index.vue')
            },
            {
                path: '/experience/uploadAndExport',
                name: 'uploadAndExport',
                component: () => import('@/views/experienceZone/uploadAndExport/index.vue')
            },
            {
                path: '/experience/magic',
                name: 'magic',
                component: () => import('@/views/experienceZone/magicSquare/index.vue')
            },
            {
                path: '/experience/transition',
                name: 'transition',
                component: () => import('@/views/experienceZone/transition/index.vue')
            },
            {
                path: '/experience/trends',
                name: 'trends',
                component: () => import('@/views/experienceZone/trends/index.vue')
            },
            {
                path: '/experience/bigTable',
                name: 'bigTable',
                component: () => import('@/views/experienceZone/bigTable/index.vue')
            },
            {
                path: '/experience/dialog',
                name: 'dialog',
                component: () => import('@/views/experienceZone/dialog/index.vue')
            },
            {
                path: '/experience/news',
                name: 'news',
                component: () => import('@/views/experienceZone/news/index.vue')
            },
            {
                path: '/experience/editor',
                name: 'editor',
                component: () => import('@/views/experienceZone/editor/index.vue')
            },
            {
                path: '/experience/random',
                name: 'random',
                component: () => import('@/views/experienceZone/random/index.vue')
            },
            {
                path: '/experience/car',
                name: 'car',
                component: () => import('@/views/experienceZone/car/index.vue')
            },
            {
                path: '/experience/imgPaste',
                name: 'imgPaste',
                component: () => import('@/views/experienceZone/imgPaste/index.vue')
            },
            {
                path: '/experience/pdf',
                name: 'pdf',
                component: () => import('@/views/experienceZone/pdf/index.vue')
            },
            {
                path: '/experience/word',
                name: 'word',
                component: () => import('@/views/experienceZone/word/index.vue')
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/login.vue')
    },
    {
        path: '/typeHome',
        name: 'typeHome',
        component: () => import('@/views/typeHome/index.vue')
    },
    {
        path: '/404',
        name: '404',
        component: () => import('@/views/notFound/index.vue')
    },
    //匹配所有路径vue2使用*，vue3使用/:pathMatch(.*)*或/:pathMatch(.*)或/:catchAll(.*)
    {
        path: '/:pathMatch(.*)',
        name: 'notFonud',
        redirect: '/404'
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})
router.addRoute('Home', { path: '/demo', name: 'demo', component: () => import('@/views/experienceZone/word/index.vue') })
router.beforeEach((to, from, next) => {
    // console.log(router)
    // 判断有没有登录
    if (!localStorage.getItem('token')) {
        if (to.name == 'login') {
            next()
        } else {
            ElMessage.error('登录已失效，请重新登录!')
            // router.push('login')
            next({ path: '/login' })
        }
    } else {
        next()
    }
})
export default router
