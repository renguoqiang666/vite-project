/**
 * 代码提交脚本
 */
const { exec } = require('child_process');
const commitMsg = process.argv[2];
exec(`git add . && git commit -m "${commitMsg}" && git push`, (err, stdout, stderr) => {
    if (err) {
      console.error('代码有错或暂无提交内容！');
      return;
    }
    console.log(stdout);
  });
  