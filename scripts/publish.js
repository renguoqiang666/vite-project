/**
 * 打包和代码提交脚本
 */
const { exec } = require('child_process');
const commitMsg = process.argv[2];
exec(`npm run build && git add . && git commit -m "${commitMsg}" && git push`, (err, stdout, stderr) => {
    if (err) {
      console.error('代码有错或暂无修改');
      return;
    }
    console.log(`您更新的(${commitMsg})正在提交，请稍等！`)
    console.log(stdout);
  });
  