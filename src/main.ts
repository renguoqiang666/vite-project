import { createApp, provide, reactive } from 'vue'
import './style.css'
import '@/style/base.css'
import App from './App.vue'
import router from './router/index'
import store from './store/index'
import pinia from './stores/index'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import locale from 'element-plus/lib/locale/lang/zh-cn'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import './utils/ajax'
import ClickOutside from './directives/clickoutside'
import '@/assets/font/font.css'
import '@/assets/font/iconfont.css'
import ComDialog from './views/experienceZone/dialog/components/dialog.vue'
const app = createApp(App)
// 全局注册icon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 全局注册弹框组件
app.component('ComDialog', ComDialog)
// 全局注册自定义指令
app.directive('click-outside', ClickOutside)
// ElementPlus
app.use(ElementPlus, { locale })
// 路由、仓库
app.use(router).use(store).use(pinia).mount('#app')
const user = reactive({
  name: 'rgq',
  age: 23
})
const setUser = () => {
  user.name = 'rgq1'
  user.age = 18
}
// 依赖注入
app.provide('user', {
  user,
  setUser
})
