import { defineStore } from 'pinia'
const useAge = defineStore('age', {
  state: () => ({
    age: 18
  }),
  actions: {
    changeAge(age: number) {
      this.age = age
      console.log('123')
    }
  },
  // 开始数据持久化
  persist: {
    key: 'ageStore',
    paths: ['age']
  }
})
export default useAge
