export interface ResponseType<D = Record<string, never>> {
  error_code: number
  reason: string
  result: D
}
export interface ResData<T> {
  config: any
  headers: any
  request: any
  status: number
  statusText: string
  data: T
}
