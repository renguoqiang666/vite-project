// import Axios, { AxiosRequestConfig } from 'axios'
import axios, { AxiosRequestConfig } from 'axios'
import { ElMessage } from 'element-plus'

const baseURL = ''

// const axios = Axios.create({
//   baseURL,
//   timeout: 30 * 1000, // 请求超时
//   headers: {
//     'Content-Type': '	application/x-www-form-urlencoded'
//   },
// })
axios.defaults.timeout = 60 * 1000
axios.defaults.baseURL = '/'

// 请求拦截器（发起请求之前的拦截）
axios.interceptors.request.use(
  config => {
    /**
     * 根据你的项目实际情况来对 config 做处理
     * 这里对 config 不做任何处理，直接返回
     */
    const token = localStorage.getItem('token')
    token && (config.headers.Authorization = `Bearer ${token}`)
    config.headers['time'] = '23243'
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 后置拦截器（获取到响应时的拦截）
axios.interceptors.response.use(
  response => {
    /**
     * 根据你的项目实际情况来对 response 和 error 做处理
     * 这里对 response 和 error 不做任何处理，直接返回
     */
    console.log(response)
    if (response.config.method === 'get') {
      ElMessage.success('获取成功')
    }
    return response
  },
  error => {
    if (error.response && error.response.data) {
      const code = error.response.status
      const msg = error.response.data.message
      ElMessage.error(`目前只支持本地调用接口 Code: ${code}, Message: ${msg}`)
    } else {
      ElMessage.error(`目前只支持本地调用接口${error}`)
    }
    return Promise.reject(error)
  }
)

export default axios
